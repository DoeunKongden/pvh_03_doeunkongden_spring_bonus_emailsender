package com.example.testmailsender.controllers;

import com.example.testmailsender.model.EmailDetails;
import com.example.testmailsender.service.EmailService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailSenderController {
    private final EmailService emailService;

    public EmailSenderController(EmailService emailService) {
        this.emailService = emailService;
    }

    @PostMapping("/send-simple-email")
    public String sendEmail(@RequestBody EmailDetails details){
        String status = emailService.sendSimpleMail(details);
        return status;
    }

    @PostMapping("/send-email-with-attachment")
    public String sendEmailWithAttachment(@RequestBody EmailDetails emailDetails){
        String status  = emailService.sendMailWithAttachment(emailDetails);
        return status;
    }


}
