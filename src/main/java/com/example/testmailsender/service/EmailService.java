package com.example.testmailsender.service;

import com.example.testmailsender.model.EmailDetails;
import jakarta.mail.MessagingException;

import java.util.Map;

public interface EmailService {


    //Send Simple Email
    String sendSimpleMail(EmailDetails details);

    //Send Email With Attachment
    String sendMailWithAttachment(EmailDetails details);
}
