package com.example.testmailsender.service.serviceImplement;

import com.example.testmailsender.model.EmailDetails;
import com.example.testmailsender.service.EmailService;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;

import java.io.File;

@Service
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender javaMailSender;
    @Value("${spring.mail.username}")
    private String sender;

    private final TemplateEngine templateEngine;

    public EmailServiceImpl(JavaMailSender javaMailSender, TemplateEngine templateEngine) {
        this.javaMailSender = javaMailSender;
        this.templateEngine = templateEngine;
    }


    //Implementing 1 method just sending simple email
    @Override
    public String sendSimpleMail(EmailDetails details) {

        //Checking if there is exception
        try {
            SimpleMailMessage mailMessage = new SimpleMailMessage();

            //Setting up details for sending mail
            mailMessage.setFrom(sender);
            mailMessage.setTo(details.getRecipient());
            mailMessage.setText(details.getBody());
            mailMessage.setSubject(details.getSubject());

            //Sending the email
            javaMailSender.send(mailMessage);
            return "Mail Have Been Send Successfully";
        } catch (Exception e) {
            return "Error While Sending Email";
        }
    }

    @Override
    public String sendMailWithAttachment(EmailDetails details) {

        //Mime Message
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper;


        try {
            mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            System.out.println(details.getAttachment());

            //String Html
            String html = "<!doctype html>\n" +
                    "<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\"\n" +
                    "      xmlns:th=\"http://www.thymeleaf.org\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <meta name=\"viewport\"\n" +
                    "          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">\n" +
                    "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n" +
                    "    <title>Email</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div> <h1>" + details.getSubject() + "</h1></div>\n" +
                    "\n" +
                    "<div> <p>" + details.getBody() + "</p></div>\n" +
                    "<img src='"+details.getAttachment()+"'/>"+
                    "\n" +
                    "<div> <p>Yours Truly</p></div>\n" +
                    "\n" +
                    "</body>\n" +
                    "</html>\n";

            mimeMessageHelper.setFrom(sender);
            mimeMessageHelper.setTo(details.getRecipient());
            mimeMessageHelper.setText(html,true);
            mimeMessageHelper.setSubject(details.getSubject());

            //adding attachment
            FileSystemResource fileResource = new FileSystemResource(new File(details.getAttachment()));
            mimeMessageHelper.addAttachment(fileResource.getFilename(), fileResource);

            //sending email
            javaMailSender.send(mimeMessage);
            return "Send Email With Attachment successfully";
        } catch (MessagingException e) {
            return "Error While Sending Email With Attachment";
        }
    }
}
